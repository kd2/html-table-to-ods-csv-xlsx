release:
	mkdir -p KD2/HTML
	for i in TableExport TableToCSV TableToODS TableToXLSX CSSParser; do \
		wget -O KD2/HTML/$${i}.php https://fossil.kd2.org/kd2fw/doc/trunk/src/lib/KD2/HTML/$${i}.php; \
	done;