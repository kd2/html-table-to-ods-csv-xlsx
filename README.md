# HTML Table to ODS/CSV/XLSX

This package allows you to convert HTML tables to ODS (LibreOffice), CSV, or XLSX (Excel) spreadsheets, including CSS support.

* Very lightweight, no dependencies
* Each table creates a different sheet in the document
* Support for rowspan and colspan
* Automatic support for numbers, currency, dates, percentage, strings
* Automatic columns width
* You can force cell type
* Support for basic CSS (using our own CSS parser): colors, backgrounds, padding, italic, bold, font size, text align, vertical align, borders, wrap, hyphens, text rotation (ODS only)

See code for documentation.

What is NOT supported:

- cells using rowspan AND colspan at the same time
- formulas

## Development

Development is at <https://fossil.kd2.org/kd2fw/>. This repository is just a mirror for Composer.

## License

GNU Affero GPL v3.

Contact us at <contact@kd2.org> for another custom license.
